
## Atividade de replicação de banco de dados
Este repositório contém uma aplicação java de cadastro de pessoas, utilizando tecnologia **RMI** para comunicação entre a aplicação cliente e o servidor, e Mysql como banco de dados.

O Servidor se conecta a dois bancos, sendo um deles **master** e o outro **slaver**. O master é usado para realização das inserções e o **slaver** para consulta e replicação. No **slaver** o master replica seus dados, assim é feito um backup automático do banco.
### Arquitetura
![enter image description here](https://gitlab.com/danielarraiscarvalho/cadastrodepessoasrmi/raw/master/arquitetura.png)
### Configuração dos bancos
Para configurar os dois banco, basta seguir o tutorial deste [link](https://www.alibabacloud.com/blog/how-to-configure-mysql-master-slave-replication-on-ubuntu-16-04_593982). 

**Obs.:** use ubuntu 16.04.

### Configuração das conexões
Para alterar as configurações de conexão, basta acessar este [arquivo](https://gitlab.com/danielarraiscarvalho/cadastrodepessoasrmi/blob/master/ServidorRMI/src/main/java/ConnectionFactory.java) e alterar os seguinte trechos de código de acordo com suas configurações de seus bancos:

    //Congigurações do Master
    String host = "172.17.0.2";
    String database = "cadastrormi";
    String user = "replica";
    String password = "replica";
    
    //Configurações do Slaver
    String host = "172.17.0.3";
    String database = "cadastrormi";
    String user = "consulta";
    String password = "consulta";
### Conexão remota entre o servidor e o cliente via RMI
Para usar o cliente e o servidor em máquinas diferentes, deve-se adicionar as seguintes linhas de código neste [arquivo](https://gitlab.com/danielarraiscarvalho/cadastrodepessoasrmi/blob/master/ServidorRMI/src/main/java/RMIServidor.java) antes da inicialização da instância do Servidor:

    //Configurações para permitir conexão remota
    System.setProperty("java.rmi.server.hostname", "<IPDAMAQUINA>");
    System.setProperty("java.security.policy", "server.policy");

Essas duas linhas alteram a configuração da JVM em tempo de execução, permitindo que os métodos do objetos carregados no RMI sejam acessados a partir de outras máquinas da rede.
## Sobre
Trabalho realizado para obtenção de nota na disciplina de Sistemas Distribuídos, do curso de Sistemas de Informação da Faculdade Católica do Tocantins, semestre 2018/2.

Disciplina ministrada pelo professor **Fredson Vieira Costa**.
### Desenvolvedores 
 1. Daniel Arrais
 2. Emerson
 3. Thiago
 4. Adailson Aguiar
