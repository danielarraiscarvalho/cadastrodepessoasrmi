import java.rmi.AlreadyBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

public class Servidor implements PessoaDao {

    private PessoaDAOImpl pessoaDAOImpl;

    public Servidor() {
        pessoaDAOImpl = new PessoaDAOImpl();
    }

    @Override
    public void inserir(Pessoa pessoa) {
        new Thread(() -> pessoaDAOImpl.inserir(pessoa)).start();
    }

    @Override
    public void deletar(int id) {
        new Thread(() -> pessoaDAOImpl.deletar(id)).start();
    }

    @Override
    public void alterar(Pessoa pessoa) {
        new Thread(() -> pessoaDAOImpl.alterar(pessoa)).start();
    }

    @Override
    public Pessoa buscar(int id) {
        return pessoaDAOImpl.buscar(id);
    }

    public void listen() {
        Servidor servidor = new Servidor();

        try {
            PessoaDao stub = (PessoaDao) UnicastRemoteObject.exportObject(servidor, 0);

            Registry registry = LocateRegistry.createRegistry(1099);
            registry.bind("persistencia_pessoas", stub);

            System.out.println("Servidor iniciado!");
        } catch (RemoteException | AlreadyBoundException e) {
            System.out.println("Falha ao iniciar servidor: "+e.getMessage());
        }
    }

}
