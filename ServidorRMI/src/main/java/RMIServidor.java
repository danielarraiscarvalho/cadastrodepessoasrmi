/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

public class RMIServidor {
    public static void main(String[] args) {

        //Configurações para permitir conexão remota
        System.setProperty("java.rmi.server.hostname", "192.168.137.47");
        System.setProperty("java.security.policy", "server.policy");

        Servidor servidor = new Servidor();
        servidor.listen();
    }
}
