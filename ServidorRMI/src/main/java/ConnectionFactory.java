import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ConnectionFactory {

    private Connection conecxaoMaster;
    private Connection conexaoSlave;

    public Connection getConnection(ConnectionType type) {
        if (type == ConnectionType.INSERT_DELETE_UPDATE) {
            return getConecxaoMaster();
        } else {
            return getConexaoSlave();
        }
    }

    private Connection getConecxaoMaster() {
        try {
            if (conecxaoMaster == null || conecxaoMaster.isClosed()) {

                String host = "172.17.0.2";
                String database = "cadastrormi";
                String user = "replica";
                String password = "replica";

                conecxaoMaster = DriverManager.getConnection("jdbc:mysql://" + host + "/" + database, user, password);
            }
        } catch (SQLException ex) {
            Logger.getLogger(ConnectionFactory.class.getName()).log(Level.SEVERE, null, ex);
        }
        return conecxaoMaster;
    }

    private Connection getConexaoSlave() {
        try {
            if (conexaoSlave == null || conexaoSlave.isClosed()) {

                String host = "172.17.0.3";
                String database = "cadastrormi";
                String user = "consulta";
                String password = "consulta";

                conexaoSlave = DriverManager.getConnection("jdbc:mysql://" + host + "/" + database, user, password);
            }
        } catch (SQLException ex) {
            Logger.getLogger(ConnectionFactory.class.getName()).log(Level.SEVERE, null, ex);
        }

        return conexaoSlave;
    }
}
