import java.rmi.Remote;
import java.rmi.RemoteException;

interface PessoaDao extends Remote {
    void inserir(Pessoa pessoa) throws RemoteException;
    Pessoa buscar(int id) throws RemoteException;
    void deletar(int id) throws RemoteException;
    void alterar(Pessoa pessoa) throws RemoteException;   
}
